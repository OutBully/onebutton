//
//  ViewController.swift
//  OneButtonSwift
//
//  Created by Jaxon Stevens on 6/28/17.
//  Copyright © 2017 Jaxon Stevens. All rights reserved.
//

// Develop Branch
// Develop Branch Again
import UIKit

class ViewController: UIViewController {

    //var myName = "Jaxon "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Hello There")

        // Do any additional setup after loading the view, typically from a nib.
    }

   
    @IBOutlet weak var buttton1Outlet: UIButton!
    @IBOutlet weak var button2Outlet: UIButton!
    @IBOutlet weak var myLabel: UILabel!
    
    @IBAction func button1(_ sender: Any) {
        
       myLabel.text = "Button One"
        
       // print("Hello There" + myName)

    }
    
    
    @IBAction func button2(_ sender: Any) {
        
        
        myLabel.text = "Button Two"
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
  

}

